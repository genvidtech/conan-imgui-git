from conans import ConanFile, CMake, tools
import os
import shutil

class IMGUIConan(ConanFile):
    name = "imgui"
    version = "1.60"
    url = "https://bitbucket.org/genvidtech/conan-imgui"
    homepage = "https://github.com/ocornut/imgui"
    description = "Bloat-free Immediate Mode Graphical User interface for C++ with minimal dependencies."
    license = "MIT"
    exports = ["LICENSE.md"]
    exports_sources = ["CMakeLists.txt", "imgui_demo.h", "imgui_demo.cpp"]
    generators = "cmake"
    settings = "os", "compiler", "build_type", "arch"
    source_subfolder = "source_subfolder"

    def source(self):
        self.run("git clone https://github.com/ocornut/imgui")
        self.run("cd imgui && git checkout v%s" % self.version)
        shutil.copy("CMakeLists.txt", "imgui/CMakeLists.txt")

    def build(self):
        cmake = CMake(self)
        self.run('cmake imgui %s' % (cmake.command_line,))
        self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy(pattern="LICENSE.txt", dst="license", src=self.source_subfolder)
        self.copy("*.h", dst="include", src="imgui")
        self.copy("*imgui.lib", dst="lib", keep_path=False)
        self.copy("*imgui.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["imgui"]
